# This part of Sub-Branch of the Project is used to host and collaborate on code, track issues, and continuously build, test, and deploy web app with Manual Builds

## OPENSOURCE NOTICE: This is a Public repo with Private content intended for collabration intiation purpose. Content shouldn't be used,published,modified in any sorts without the jointly written consent of the Owner&Contact-Base Provider.


### Abilities for this part of Sub-branch:
anyone can,
- contribute business visiting cards/contacts
- can edit the content page styling
- can add hyperlinks to their Business
- contribute the content alignment

### prerequisites (for this sub-branch):
knowlege of
- git
- markdown (markup language)

```sh
### prerequisites (for main-project):
knowlege of
- git
- html
- css
- js
- ts
- git modules
- gimp
- xml
- json
- tailwind css
- go language


### Features of Main Project:
- smooth transitions
- app style
- scalabe
- fast
- searchable titles
- lazy load function
- dark and light mode
- multilingual
- content expiration note
- responsive design
```

# CONCEPT,Manual Builds,FrontEnd Development using opensource-ecosystem by _Thiru.Tilak.T_ & Contact-Base provided by Thiru.Mugunth K

## Invite: Collabrators welcome.
