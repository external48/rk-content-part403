---
title: "Agri"
date: 2022-09-18T16:05:20+05:30
draft: false
slug: pharma
featured_image: "https://source.unsplash.com/random/1000x500/?farm"
summary: Business related to Agri
layout: gallery
tags:
  - Sidda
  - Organic
categories:
  - Aathi
---

{{< gallery >}}

![](1d2c4c25-bad4-4963-9ac9-5ad34c11a57f-86.webp)
![](6c6911d0-0060-4ad0-bcb5-82e4da9abeb1-89.webp)

![](65bc50ef-1718-4a97-b96c-914408971f8b-61.webp)
![](242160256_23849054919250083_193244975368451459_n.png-84.webp)

![](305043783_2257386721086113_6729751591299254968_n-58.webp)
![](ad-95.webp)
![](arjun%20thondaimaar-90.webp)
![](asdf-88.webp)
![](COMPRESSOR-94.webp)

![](eca03e89-0470-4ef4-b586-e47988d69bcd-60.webp)
![](fise-85.webp)
![](fish%201-93.webp)
![](photo_2022-01-29_17-13-01-1.webp)
![](sd-87.webp)
![](theevanam-92.webp)
![](tour%20-%20Copy-91.webp)
![](Untitled-82.webp)
![](Untitledd-83.webp)
![](WhatsApp%20Image%202022-01-17%20at%207.20.39%20PM%20(1)-20.webp)
{{< /gallery >}}