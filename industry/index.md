---
title: "Industry"
date: 2022-09-18T16:06:20+05:30
draft: false
slug: industry
featured_image: "https://source.unsplash.com/random/1000x500/?industrial"
summary: Business related to Industries
layout: gallery
tags:
  - Industrial
  - Pumps
  - Thanjavur
categories:
  - Aathi
---

{{< gallery >}}
![](1ca26a9e-c21b-41d3-8d63-ee4d68c7d4a0-117.webp)
![](mathi-116.webp)

![](mathialagan-118.webp)
![](sarasva-120.webp)
![](tart-119.webp)
{{< /gallery >}}

<!--
## Grid Gallery

> Spring, the sweet spring, is the year's pleasant king.


{{< gallery-grid >}}
![ROSE](1ca26a9e-c21b-41d3-8d63-ee4d68c7d4a0-117.webp)

![ROSE](mathi-116.webp)

![ROSE](mathialagan-118.webp)

![ROSE](sarasva-120.webp)
![ROSE](tart-119.webp)
{{< /gallery-grid >}}
-->