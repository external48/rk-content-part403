---
title: "Pharma"
date: 2022-09-18T16:05:20+05:30
draft: false
slug: pharma
featured_image: "https://source.unsplash.com/random/1000x500/?pharma"
summary: Business related to Pharma
layout: gallery
tags:
  - Sidda
  - Ayurveda
  - Allopathy
categories:
  - Aathi
---

{{< gallery >}}

![](517b0157-bf23-4a05-a727-a6b51fdea108-42.webp)
![](46023319-54a1-4332-94bb-dccf5345f976-40.webp)

![](ASDAFD-27.webp)
![](pharmaa-41.webp)


{{< /gallery >}}