---
title: "Dress"
date: 2022-09-18T16:08:20+05:30
draft: false
slug: dress
featured_image: "https://source.unsplash.com/random/1000x500/?saree+clothing"
summary: Business related to Garments
layout: gallery
tags:
  - Silks
  - Sarees
  - Garments
categories:
  - Aathi
---

{{< gallery >}}

![](b37583c4-6af6-4792-b0ad-f47e8cbe79b5-18.webp)

![](d01d0f17-45c1-4682-add0-9bd669906884-17.webp)
![](dress-73.webp)
{{< /gallery >}}